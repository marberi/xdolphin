#!/usr/bin/env python

import imp
import os

from . import topnode
from . import layers

def plot(name):
    return Plot(name)

class Plot:
    def __init__(self, plot_name):
        if isinstance(plot_name, str):
            path = '{0}.py'.format(plot_name)
            mod = imp.load_source(plot_name, path)
            plot_cls = getattr(mod, 'Plot')
        else:
            plot_cls = plot_name

        self.plot_inst = plot_cls()

    @property
    def pipel(self):
        return self.plot_inst.pipel()

    @property
    def prep(self):
        inst = self.plot_inst
        pipel = inst.pipel()
        prep = inst.prepare(pipel)

        return prep

    @property
    def topnode(self):
        inst = self.plot_inst
        if hasattr(inst, 'topnode'):
            top = self.plot_inst.topnode()
        elif hasattr(inst, 'pipel'):
            pipel = inst.pipel()
            top = topnode.Topnode(pipel, confs={})

        return top

    def _default_show_order(self):
        """Create the plot in a default order"""

        inst = self.plot_inst
        if hasattr(inst, 'topnode'):
            top = inst.topnode()
        elif hasattr(inst, 'pipel'):
            pipel = inst.pipel()
            tmp = layers.Layers()
            tmp.add_pipeline(pipel)

            top = tmp.to_topnode()
        else:
            raise RuntimeError('no topnode defined')

        prep = top.reduce(inst.f_reduce)
        inst.plot(prep)

    def show(self, file_name=None, call_method=None):
        from matplotlib import pyplot as plt

        inst = self.plot_inst
        if not call_method:
            self._default_show_order()
        else:
            msg = '{0} is not a method on the plot instance'.format(call_method)
            assert hasattr(inst, call_method), msg

            getattr(inst, call_method)()

        if file_name:
            path = os.path.join('..', 'run', file_name)
            plt.savefig(path)
        else:
            plt.show()
