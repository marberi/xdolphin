#!/usr/bin/env python
# Default paths unless something else is specified.
# Users are not supposed to use this functionality, but
# to specify the configuration elsewhere [specify.]

# meta - Save information of what is actually calculated.


# These need to be defined in the correct order.
default_dirs = [\
  ('root', '~/.xdolphin'),
  ('cache', ('root', 'cache')),
  ('archive', ('root', 'archive')),
  ('tmp', ('root', 'tmp')),
  ('interf', ('cache', 'interf')),
  ('meta', ('cache', 'meta')),
  ('prepare', ('root', 'prepare'))]

proj_dirs = [('task_path', 'task'), ('fig_path', 'fig')]
