#!/usr/bin/env python

import json
import os
import sys

import pandas as pd

from . import Job

def mk_pipel(nodes, dep):
    """Construct the pipeline graph from a data structure."""

    nodesD = {}
    for jobid, row in nodes.iterrows():
        task = Job(row['name'])
        task.config.update(row.config)

        nodesD[int(jobid)] = task

    for key, row in dep.iterrows():
        j_from = nodesD[row['from']]
        j_to = nodesD[row['to']]

        j_from.depend[row['name']] = j_to

    root_jobid = int(nodes[nodes.taskid == taskid].index[0])

    job = nodesD[root_jobid]

    return job

def load_pipel(path): #task_dir, taskid):
    """Load task from cached version on file."""

    def load_part(x):
#        path = os.path.join(task_dir, taskid, '{0}.json'.format(x))
        res = json.load(open(path))
        df = pd.DataFrame(res).T

        return df

    nodes = load_part('nodes')
    dep = load_part('dep')

    job = mk_pipel(nodes, dep)

    return job

