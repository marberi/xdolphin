#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger as ipdb
import glob
import os
import shutil
import tempfile

import pandas as pd
import xarray as xr

class Input_one_job:
    """Wrapper around results so the tasks doesn't have access
       to all previous tasks.
    """

    def __init__(self, file_path):
        self.file_path = file_path

    def _get_result(self):
        """Fetch the default DataFrame from the defaults store."""

        # We don't have any metadata saying if Pandas or xarray
        # was used to create file. Just try.
        try:
            result = pd.read_hdf(self.file_path, 'default', mode='r')
#            store = pd.HDFStore(self.file_path, mode='r')
#            result = store['default']
#            store.close()
            is_pandas = True
        except TypeError:
            store.close()
            is_pandas = False

        if not is_pandas:
            dataset = xr.open_dataset(self.file_path)
            result = dataset.default if 'default' in dataset else dataset


        return result

    def _set_result(self):
        raise ValueError('This is not allowed..')

    def get_store(self, mode='r'):
        """Get the store.."""

        # Otherwise we might risk creating empty files.
        assert os.path.exists(self.file_path)
        store = pd.HDFStore(self.file_path, mode=mode)

        return store

    result = property(_get_result, _set_result)

class Input_wrapper:
    """Interface so the task only have access to the relevan input."""

    # This is moving in the direction of the jobs not having directly
    # access to all the previous information. All the tasks should have
    # been coded in this manner, but this is not always realized.
    def __init__(self, config, D):
        self.config = config

        for x,y in D.items():
            setattr(self, x, y)

        self.depend = D

class Output_wrapper:
    """Wrapper around the output."""

    # A bit different since it does not know the taskid, but
    # only deal with relative storage..
    def __init__(self, tmp_dir):
        self._tmp_dir = tmp_dir
        self._to_sync = {}

    def empty_file(self, tag): #, taskid):
        """Return path to where one can store the results."""

        tmp_path = tempfile.mktemp(dir=self._tmp_dir)
        self._to_sync[tag] = tmp_path

        return tmp_path

    def _get_result(self):
        raise ValueError('Only setting the resut is allowed..')

    def _set_result(self, result):
        assert not 'default' in self._to_sync, 'Already set..'
        tmp_path = tempfile.mktemp(dir=self._tmp_dir)

        if isinstance(result, dict):
            store = pd.HDFStore(tmp_path)
            for key, val in result.items():
                store.append(key, val)
        elif isinstance(result, pd.DataFrame):
            store = pd.HDFStore(tmp_path)
            store.append('default', result)
        elif isinstance(result, pd.Series):
            store = pd.HDFStore(tmp_path)
            store.append('default', result)
        elif isinstance(result, xr.DataArray):
            # We don't yet know how to handle reading these.

            S = result.to_dataset('default')
            S.to_netcdf(tmp_path)
        else:
            raise ValueError('Input type not supported..')

        self._to_sync['default'] = tmp_path

    result = property(_get_result, _set_result)

class Archive:
    """Interface so the jobs are not directly dealing with the
       local file system.
    """

    def __init__(self, config):
        self.config = config

        self._archive_dir = self.config['dirs']['archive']
        self._tmp_dir = self.config['dirs']['tmp']

        # Should act like a frozen list...
        self.finished = self._load_finished()

    def _load_finished(self):
        """List of finished tasks."""

        g = os.path.join(self._archive_dir, '*', '*')
        finished = list(set(map(os.path.basename, glob.glob(g))))
   
        return finished 

    def result_path(self, tag, taskid):
        """Returns a path to the result."""

        file_path = os.path.join(self._archive_dir, tag, taskid)
        if not os.path.exists(file_path):
            raise ValueError('No such result in archive:', tag, taskid)

        return file_path


    def input_wrapper(self, job):
        """Create an input wrapper for a specific job.."""

        print('in result_wrapper...')
        D = {}
        for key, dep in job.depend.items():
            result_path = self.result_path('default', dep.taskid())
            D[key] = Input_one_job(result_path)

        wrapper = Input_wrapper(job.config, D)

        return wrapper

    def output_wrapper(self, job):
        # Here I can later assign a single directory to all the
        # different tasks..

        return Output_wrapper(self._tmp_dir)

    def store_frametbl(self, df, tag, taskid, field):
        print('Triggering store_frametbl')
        tmp_path = self.empty_file(tag, taskid)
        store = pd.HDFStore(tmp_path, 'w')
        store.append(field, df)
        store.close()

    def store_fixed(self, df, tag, taskid, field):
        # Should only be used when frametables are not working.
        tmp_path = self.empty_file(tag, taskid)
        store = pd.HDFStore(tmp_path, 'w')
        store[field] = df
        store.close()

    def commit(self, output, taskid):
        # Should perhaps not be here...

        objid = taskid
        for tag, from_path in output._to_sync.items():
            tagpath = os.path.join(self._archive_dir, tag)
            objpath = os.path.join(tagpath, objid)

            shutil.move(from_path, objpath)

        self.finished.append(taskid)
