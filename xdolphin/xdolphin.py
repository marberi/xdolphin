#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

from IPython.core import debugger as ipdb
import copy
import json
import os
import re
import shutil
import sys
import time
import numpy as np
import pandas as pd
from pathlib import Path

from . import obj_hash
from . import mainconfig
from . import ntasks
from . import libtaskid
from . import Common

conf = mainconfig.MainConfig()
tasks = ntasks.Tasks(conf)

class Config:
    """Interface for the task configuration. Currently mostly simple 
       checks.
    """

    def __init__(self, job, task_config):
        self.job = job
        self.task_config = task_config

        self.__repr__ = self.task_config.__repr__

    def __getitem__(self, key):
        if not '.' in key:
            return self.task_config[key]

        spl = key.split('.')
        node = self.job

        for key in spl[:-1]:
            node = node.depend[key]

        return node.config[spl[-1]]

    def _flatten_key(self, key):
        if isinstance(key, tuple):
            return '.'.join(map(self._flatten_key, key))
        else:
            return str(key)

    def _find_jobs(self, node, inds=''):
        """Return all keys with a flattened key."""

        # Since the keys can have a funky hirarchical structure. Later I
        # might restrict that.
        key_jobD = {}
        for key, dep_job in iter(node.depend.items()):
            flat_key = self._flatten_key((inds,key) if inds else key)

            key_jobD[flat_key] = dep_job
            key_jobD.update(self._find_jobs(dep_job, flat_key))

        return key_jobD

    def keys(self):
        return self.task_config.keys()

    def _set_wildcard(self, key, val):
        """Setting configuration options using wildcards."""

        # Need to construct a regex through the steps
        # 1 - Exclude the last part. This is the parameter.
        # 2 - Replace . and * have other meaning in regex. Use
        #     . -> \. and * _ .*? since . is a litteral and we
        #     want a non-greedy regex.
        
        spl = key.split('.') # Part 1
        assert 2 <= len(spl)
        pattern = '.'.join(spl[:-1])

        TMP = 'TMP_HACK' # Part 2
        pattern = pattern.replace('.', TMP).replace('*', '.*?').replace(TMP, "\\.")
        f = re.compile(pattern).match

        key_jobD = self._find_jobs(self.job)

        # Here we avoid setting a configuration more than once. While the
        # result would not change, it avoids operations.
        # This part avoid se
        S = []
        par = spl[-1]
        for x, job in iter(key_jobD.items()):
            if f(x) and (not job.jobid in S):
                job.config[par] = val
                S.append(job.jobid)

    def __setitem__(self, key, val):


        # The wildcard code is currenly more complicated. Only
        # using this path when needed.
        if '*' in key:
            self._set_wildcard(key, val)
            return

        if not '.' in key:
            assert key in self.task_config, 'No such option: {0}'.format(key)
            self.task_config[key] = val
            return

        spl = key.split('.')
        node = self.job
        for key in spl[:-1]:
            node = node.depend[key]

#        assert not isinstance(

        assert spl[-1] in node.config.keys()
        node.config[spl[-1]] = val

 
    def update(self, new_conf):

        # Not testing if the options are actually valid.
        new_style = True in set([('.' in x) for x in new_conf.keys()])
        has_wildcard = True in set([('*' in x) for x in new_conf.keys()])

        assert not has_wildcard, 'This options is not tested/implemented.'

# Comment to disable checking: 
#
#        wrong_keys = set(new_conf.keys()) - set(self.task_config.keys())
#        assert not len(wrong_keys), 'Invalid keys: {0}'.format(', '.join(list(wrong_keys)))

#        if not new_style:
#            self.task_config.update(new_conf)
#            return

        for key,val in iter(new_conf.items()):
            self[key] = val

class Depend(dict):
    """Object handling the job dependencies."""

    # These methods should be elsewhere, but this is simpler.
    def __call__(self, name, job):
        raise NotImplementedError

    def __getitem__(self, key):
        """Recursively resolve the names."""

        # TODO: This code need to be updated to find groups.
        get = super(Depend, self).__getitem__
        if not '.' in key:
            return get(key)

        spl = key.split('.')
        node = get(spl[0])
        for x in spl[1:]:
            node = super(Depend, node.depend).__getitem__(x)

        return node

    def __setitem__(self, key, val):
        # TODO: Test if the value is a job..
        super(Depend, self).__setitem__(key, val)

        # Also set these entries as attributes on the Job. Ignore cases
        # not working
        try:
            setattr(self.job, key, val)
        except TypeError:
            pass

class Job(object):
    """Creates a job with all the configurations."""

    def __init__(self, task_cls='empty', session=None, depend={}):
#            assert task_cls in tasks, 'No such task: {0}'.format(task_cls)
        task = tasks[task_cls]()

        # Ok, there should be a better way to do this.
        task.config = copy.deepcopy(task.config)

        self._check_task(task)

        self.task = task
        self.task.job = self

        self.name = self.task.name
        self.jobid = id(self)
        self.config = Config(self, self.task.config)

        if not isinstance(session, type(None)):
            self.session = session

        # Note: Pass the job explicitly.
        self.depend = Depend(depend)
        self.depend.job = self

    def _check_task(self, task):
        assert hasattr(task, 'config'), 'Missing configuration.'

    def create_child(self):
        ipdb.set_trace()

    def log(self):
        ipdb.set_trace()

    def run(self, use_cache=True):
        # This functionality is moved into a different class.

        archive = self.session.archive
        self.session.runner(self, archive, use_cache)

    def run_one(self):
        """Run a single job without the dependencies."""

        archive = self.session.archive
        self.session.runner.run_one(self, archive)

    def __getitem__(self, dep_name):
        return getattr(self, dep_name) 

    def __setitem__(self, key, job):

        assert hasattr(job, 'jobid'), 'One can only depend on jobs.'

        self.depend[key] = job 
        setattr(self, key, job)

    def taskid(self):
        """Unique name of the task."""
       
        # This is faster than a straight forward recursive code.
        depD, jobsD = libtaskid.find_dep(self)
        order = libtaskid.determine_order(depD)
        taskidD = libtaskid.estim_taskid(order, jobsD)

        taskid = taskidD[self.jobid]

        return taskid

    def data(self):
        """Create the dataframes for the nodes and the dependencies."""

        # Note, these steps are fast compared to actually creating
        # the dataframe (yes..).
        depD, jobsD = libtaskid.find_dep(self)
        order = libtaskid.determine_order(depD)
        taskidD = libtaskid.estim_taskid(order, jobsD)

        # DataFrame with the nodes.
        nodes = pd.DataFrame(columns=['name', 'config', 'taskid'])
        for jobid, job in jobsD.items():
            node = pd.Series()
            node['name'] = job.name
            node['taskid'] = taskidD[jobid]
            node['config'] = dict(job.config)
            nodes.loc[jobid] = node

        # Dataframe with the dependencies.
        dep = pd.DataFrame()
        for k,v in depD.items():
            part = pd.DataFrame([(k,)+x for x in v], \
                   columns=['from', 'name', 'to'])

            dep = dep.append(part, ignore_index=True)

        return nodes, dep

    def save_json2(self, path):
        """Save a pipeline to a json file."""

        # Old, but I need this part again!!

        fb = open(path, 'w')

        # Storing nodes and dependencies in the same file. This
        # is simpler for semi-manual usage (read: scp).
        nodes, dep = self.data()

        (nodes.T).to_json(fb)
        fb.write('\n')
        (dep.T).to_json(fb) 

    def save_json(self, path):
        # Experimenting with bypassing the part which is using the
        # dateframe....

        jobsD = libtaskid.jobs_graph(self)
        json.dump(jobsD, open(path, 'w'))

    def to_pic(self):
        taskid = self.taskid()

        from IPython.core import debugger as ipdb

        # Store the request.
        d = '/home/eriksen/to_pic'
        path_out = os.path.join(d, '{}.json'.format(taskid))
        self.save_json2(path_out)

        # Make the transfer to PIC.
        cmd = 'scp {} ui.pic.es:~/from_laptop/.'.format(path_out)
        os.system(cmd)

    def cache_to_pic(self, test_for=['limit_ngal']):
        nodes, dep = self.data()

        #test_for = ['model_rebin','limit_ngal']
        totransfer = nodes[nodes.name.isin(test_for)]

        d_archive = '/home/eriksen/.xdolphin/archive'
        dst = 'ui.pic.es:/nfs/astro/eriksen/.xdolphin/archive/default/.'

        os.chdir(os.path.join(d_archive, 'default'))
        file_list = ' '.join(totransfer.taskid.unique())

        command = "rsync -avrz {0} {1}".format(file_list, dst)
        os.system(command)


    def from_pic(self, taskid=None):
        """Copy cached task from PIC."""

        taskid = self.taskid() if (taskid is None) else taskid
        d_pic = '/nfs/astro/eriksen/.xdolphin/archive'
        d_local = '/home/eriksen/.xdolphin/archive'

        src_path = os.path.join(d_pic, 'default', taskid)
        dst_path = os.path.join(d_local, 'default', taskid)

        cmd = 'scp ui03.pic.es:{} {}'.format(src_path, dst_path)
        os.system(cmd)

    def run_pic(self, task_name='run_task', copy_back=True):
        """Runs the current of job remotely and fetch the result."""

        taskid = self.taskid()

        # To avoid recalculating values.
        d_local = '/home/eriksen/.xdolphin/archive'
        dst_path = os.path.join(d_local, 'default', taskid)
        if os.path.exists(dst_path):
            print('Already cached', taskid)
            return  

        # Transfers the pipeline metadata.
        self.to_pic()

        print(task_name)

        cmd = "ssh data.astro.pic.es ./proj/submit/submit.py {} {}"
        if task_name == 'run_task':
            cmd = cmd.format(taskid, 'run_task')
        else:
            cmd = cmd.format(taskid, task_name)

#            ipdb.set_trace()
        print(cmd)
        #return

        #os.system(cmd)

        # Yes, this should first test if the file exists...
        #if copy_back:
        #    self.from_pic(taskid)

    def simplify(self):
        """Replace redundant nodes."""

        # There is a reason for having the redundancy in the graph. Different
        # parts can in theory have different input, but then in practice
        # ends up being the same.

        # Note: This code is traversing the tree optimally, since the taskid
        # estimation also traverse the tree.
        def _simpl(job, has_seenD):
            for key,dep in job.depend.items():
                taskid = dep.taskid()
                if taskid in has_seenD:
                    job.depend[key] = has_seenD[taskid]
                else:
                    has_seenD[taskid] = dep
                    _simpl(dep, has_seenD)


        _simpl(self, {})

    def shallow_copy(self, with_dep=True):
        """Copy only this task and link to dependencies."""

        new_job = Job(self.name)
        new_job.config.update(copy.deepcopy(self.config.task_config))

        if with_dep:
            for key, dep in self.depend.items():
                new_job.depend[key] = dep

        return new_job

    def clone(self):
        """Clone the job. This also create new dependencies."""

        # Note: This part could use the new underlying functionaliy in
        # the data function. Creatin the dataframes is actually a 
        # bottleneck..
        nodes, dep = self.data()

        # Creatind the nodes
        xnodes = {}
        for jobid, info in nodes.iterrows():
            J = self.__class__(info['name'])
            J.config.update(info['config'])

            xnodes[jobid] = J

        # Add the dependencies
        for _, X in dep.iterrows():
            xnodes[X['from']].depend[X['name']] = xnodes[X['to']]

        return xnodes[self.jobid]


    def labels(self, labels):
        """Helper function to create graph labels for a job."""

        # Used from topnode when producing figures. Could be
        # useful in general.
        assert isinstance(labels, dict), 'labels need to be specified with a dictionary'
        labels_S = pd.Series()

        for key, lbl in iter(labels.items()):
            if isinstance(lbl, str):
                val = self.config[lbl]
            else:
                val = lbl(self)

            # There are some problems storing the booleans in Pandas.
            if isinstance(val, bool):
                val = 1*val

            labels_S[key] = val

        return labels_S

    def toempty(self):
        # Note: This is used in the PSFw project, but I don't
        # know how it works.

        # Replace the current job with an empty job, but keeps
        # the dependencies.
        njob = self.__class__('empty')

        for dep_name, dep_job in iter(self.depend.items()):
            njob.depend[dep_name] = dep_job

        return njob

 
    def __len__(self, sub_tree=False):
        """Number of jobs in the graph."""

        # TODO: This is double counting....
        taskids = [self.jobid]
        for job in self.depend.values():
            taskids += job.__len__(True)

        return len(set(taskids))

    def show(self):
        """Graphical representation of the pipeline."""

        def _display_tree(job, level):
            name = job.name
            print(' '*4*level, name)
        
            # There exists objects without dependencies...
            if not hasattr(job, 'depend'):
                return

            for key,dep in job.depend.items():
                _display_tree(dep, level+1)

        # Note: There are probably much better ways of visualizing a
        # large tree.
        _display_tree(self, 0)

    def show_rec(self, show_path=True):
        def _display_tree(job, level, pre):

            name = job.name
            if show_path:
                print(' '*4*level, name, '[', pre, ']')
            else:
                print(' '*4*level, name)
        
            # There exists objects without dependencies...
            if not hasattr(job, 'depend'):
                return

            for key,dep in job.depend.items():
                xpre = f'{pre}.{key}'
                _display_tree(dep, level+1, xpre)

        # Note: There are probably much better ways of visualizing a
        # large tree.
        _display_tree(self, 0, '')

    def _get_rec(self, job, name, L):
        """Recursice part of the code needed to find the right job."""

        if job.name == name:
            L.append(job)

        # For the Common objects..
        if not hasattr(job, 'depend'):
            return

        for key,dep in job.depend.items():
            self._get_rec(dep, name, L)

    def get(self, name):
        """Find a single task with a given name."""

        # This works with passing a list and side effects.
        L = []
        self._get_rec(self, name, L)

        assert 0 < len(L), 'No task found'

        # Needed to handle the task can appear in more locations.
        assert len(set([x.taskid() for x in L])), 'Multiple tasks found'

        return L[0]

    def _replace_rec(self, job, name, L):
        for key,dep in job.depend.items():
            if dep.name == name:
                L.append((job, key, dep))

            self._replace_rec(dep, name, L)

    def replace(self, name, new_job):
        """Replace the single occurence of a class with a new job."""

        L = []
        self._replace_rec(self, name, L)

        assert len(L), 'No task with name: {}'.format(name)
        assert len(set([x[2].taskid() for x in L])) == 1, \
               'The task to replace is not unique.'

        # Since multiple parents might depend on the same job.
        for parent, dep_name, _ in L: 
            parent.depend[dep_name] = new_job

    def replace_common(self, D, pipel=None, root=()):
        """Replace common objects with actual tasks."""

        pipel = self if isinstance(pipel, type(None)) else pipel


        for key,dep in pipel.depend.items():
            new_root = root+(key,)

            if isinstance(dep, Common):
                if not dep.name in D:
                    ipdb.set_trace()
                    raise KeyError('.'.join(new_root))

                new_job = D[dep.name]
                pipel.depend[key] = new_job

                self.replace_common(D, new_job, new_root)
            else:
                self.replace_common(D, dep, new_root)



    def path(self):
        """Returning path where the result is stored."""

        # Usually one would not use this in production, but there are
        # cases where this is useful.
        taskid = self.taskid()

        assert taskid in finished
        file_path = archive.get_path('default', taskid)

        return file_path

    # NOTE: Already cleaned up a bit..
    def _file_path(self, tag):
        """Assures the job is run and then returns the path."""

        archive = self.session.archive
        taskid = self.taskid()
        if not taskid in archive.finished:
            self.session.runner(self, archive)

        file_path = archive.result_path(tag, taskid)

        return file_path

    def _get_result(self):
        """Fetch the default DataFrame from the defaults store."""

        result = pd.read_hdf(self._file_path('default'), 'default')

#) as store:
#        with pd.HDFStore(self._file_path('default')) as store:
#            result = store['default']

        return result

    def _get_store(self):
        """Fetch a store."""

        file_path = self._file_path('default')
        store = pd.HDFStore(file_path, 'r')

        return store

    def _result_status(self):

        depD, jobsD = libtaskid.find_dep(self)
        order = libtaskid.determine_order(depD)
        taskidD = libtaskid.estim_taskid(order, jobsD)

        # Some of this should directly be at the archive.
        d = '/home/eriksen/.xdolphin/archive/default'

        # Oh yes, dirty...
        d = '/nfs/astro/eriksen/.xdolphin/archive/default'
        df = pd.DataFrame()
        for taskid in set(taskidD.values()):
            path = os.path.join(d, taskid)
            exists = os.path.exists(path)

            S = pd.Series()
            S['taskid'] = taskid
            S['path'] = path
            S['exists'] = exists
            if exists:
                S['file_size'] = os.path.getsize(path)

            df = df.append(S, ignore_index=True) 

        return df

    def backup(self, to_dir, hard_link=True):
        """Copy already calulated results elsewhere."""

        to_dir = Path(to_dir)
        fcopy = os.link if hard_link else shutil.copy

        if not to_dir.exists():
            to_dir.mkdir()

        df = self._result_status()
        for key,row in df.iterrows():
            if not row['exists']:
                continue

            to_path = to_dir / row['taskid']

            if not to_path.exists():
                fcopy(row['path'], to_path)

    def _set_result(self, df):
        """Store as the default DataFrame in the defaults store."""

        archive.store_frametbl(df, 'default', self.taskid(), 'default')


    result = property(_get_result, _set_result)
    store = property(_get_store)

    def set_session(self, session):
        """Sets the session for all subjobs."""

        # Later we might want to be a bit more careful with the
        # checks..
        self.session = session
        for dep in self.depend.values():
            dep.set_session(session)


    def status(self):
        """Print the status of which jobs has finished."""

        depD = {}
        jobsD = {}
        libtaskid.find_dep(self, depD, jobsD)
        order = libtaskid.determine_order(depD)
        taskidD = libtaskid.estim_taskid(order, jobsD)

        archive = self.session.archive
        finished = set(archive.finished)

        print('# Layer, missing, total')
        for i,jobids in enumerate(order):
            missing = {taskidD[x] for x in jobids} - finished
            if len(missing):
                print(i, len(missing), len(jobids))


    def _get_info(self):
        """Basic information about the job."""

        # Having this in __repr__ is nice when having one object, but
        # not dealing with e.g. a list of objects.
        txt = 'name: {}\n'.format(self.name)
        txt += 'depend: {}\n'.format(', '.join(self.depend.keys()))
        txt += repr(self.config.task_config)

        return txt

    def info(self):
        """Show basic information about the job."""

        print(self._get_info())

    def unique_tasks(self, L=[]):
        """Find the unique tasks in the pipeline."""

        first = not len(L)
        L.append(self.name)

        for key, dep in self.depend.items():
            dep.unique_tasks(L)

        if first:
            return set(L)

    def __call__(self, *X):
        self.task.entry(*X)

    def __repr__(self):
        return self._get_info()
