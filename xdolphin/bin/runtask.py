#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import imp
import os
import sys

class RunTask:
    def __init__(self, args):
        self.args = args

    def _find_path(self):
        plotstr = self.args.plot
        proj,plot = plotstr.split(':')

        import xdolphin
        conf = xdolphin.config.config()

        proj_path = conf['projects'][proj]['path']
        proj_path = os.path.expanduser(proj_path)
        d = os.path.join(proj_path, 'plots')

        for suf in ['py', 'yaml']:
            plot_path = os.path.join(d, '{0}.{1}'.format(plot, suf))

            if os.path.exists(plot_path):
                return suf, plot, plot_path

        raise ValueError('No such plot:', plotstr)

    def run(self):
        # Currently I require the user to specify the project
        # from the command line. In practice, this could be set
        # from another tool.

        suf, plot, plot_path = self._find_path()

        # Hack, problems with relative loading...
        sys.path.append(os.path.dirname(plot_path))
        mod = imp.load_source(plot, plot_path)

        inst = getattr(mod, plot)()
        inst.run()
