#!/usr/bin/env python
# encoding: UTF8

import pandas as pd
from pathlib import Path
import yaml

from xdolphin import mainconfig, gentaskinfo

def list_tasks():
    """Lists the available tasks."""

    config = mainconfig.MainConfig()

    cache_dir = Path(config['dirs']['cache'])
    path = cache_dir / 'interf' / 'combined.yaml'

    task_info = yaml.load(open(path))
    task_info = pd.DataFrame(task_info).T

    print(list(task_info['class']))
