#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import os
import shutil
import sys
import tempfile

import xdolphin
from xdolphin import Job

class AddFile:
    def __init__(self, args):
        self.args = args

    def input_paths(self, url):
        if url.startswith('http'):
            import urllib
            file_name = url.split('/')[-1]
            from_path = tempfile.mktemp()
      
            # The dropbox url links to a webpage. 
            if 'www.dropbox.com' in url:
                url += '?dl=1' 

            urllib.urlretrieve(url, from_path)
        else:
            from_path = url
            assert os.path.exists(from_path)
            file_name = os.path.basename(from_path)

        return from_path, file_name

    def obj_path(self, file_name):
        job = Job('filetask')
        job.task.config = {'file_name': file_name}

        taskid = job.taskid()

        return taskid

    def run(self):
        conf = xdolphin.config.config()
        archive = xdolphin.archive.localfiles.Archive(conf)

        from_path, file_name = self.input_paths(self.args.url)
        taskid = self.obj_path(file_name)

        archive.add(from_path, 'file', taskid)
