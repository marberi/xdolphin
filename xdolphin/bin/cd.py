#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import os

# This experiment did not really work....

class CD:
    def __init__(self, args):
        self.args = args

    def run(self):
        import xdolphin
        conf = xdolphin.config.config()

        proj = self.args.proj
        assert proj in conf['projects']

        path = conf['projects'][proj]['path']
        path = os.path.expanduser(path)

        print(path)
