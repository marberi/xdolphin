#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import tables

def _fname_sed(file_path, grp):
    fb = tables.open_file(file_path)
    res = {}
    for fobj in fb.get_node(grp):
        fname = fobj._v_name
        for sedobj in fobj:
            sed = sedobj.name

            res[fname, sed] = sedobj.read()

    fb.close()

    return res

# This one could also be converted to using Pandas... Would require some
# modification of the photoz code..
def load_pzcat(file_path):
    import pandas as pd

    fb = tables.open_file(file_path)
    pzcat = fb.get_node('/photoz/photoz').read()
    fb.close()

    pzcat = pd.DataFrame(pzcat)

    return pzcat

#def load_pzpdf(file_path):
#    print('Start to load pdfs')
#    with tables.open_file(file_path) as fb:
#        z_mean = fb.get_node('/z_mean').read()
#        z_width = fb.get_node('/z_width').read()
#        pdfs = fb.get_node('/pdfs').read()
#    print('pdfs loaded')
#
#    return pdfs, z_mean, z_width

def _base_pzpdf(file_path):
    pdf = {}
    with tables.open_file(file_path) as fb:
        pdf['z_mean'] = fb.get_node('/z_mean').read()
        pdf['z_width'] = fb.get_node('/z_width').read()
        pdf['pdfs'] = fb.get_node('/pdfs').read()

    return pdf

def load_pzpdf(file_path):
    return _base_pzpdf(file_path)

def load_pzpdf_type(file_path):
    return _base_pzpdf(file_path)

def _load_pandas(file_path, tag):
    import pandas as pd

    store = pd.HDFStore(file_path)
    obj = store[tag]
    store.close()

    return obj


def load_file(file_path):
    """Only gives the path to a file."""

    return file_path

def load_corr(file_path):
    import sci.pkg
    pkg = sci.pkg.PkgRead(file_path)

    return pkg

def load_maginterp(file_path):
    return _load_pandas(file_path, 'maginterp')

def load_pingupz(file_path):
    import pandas as pd

    store = pd.HDFStore(file_path)
    res = []
    for i in range(len(store)):
        res.append(store['/pingupz'+str(i)])

    store.close()

    return res

def load_thpkg(file_path):
    # Later I might consider a different way of loading
    # the results. Right now I want to start to use xdolphin
    # to interpret the results.
    import sci
    import sci.pkg
        
    pkg = sci.pkg.PkgRead(file_path)
    return pkg

# The regitered object loaders.
loaders = {'pzcat': load_pzcat,
           'pzpdf': load_pzpdf,
           'pzpdf_type': load_pzpdf_type,
           'thpkg': load_thpkg,
           'maginterp': load_maginterp,
           'file': load_file,
           'corr': load_corr,
           'pingupz': load_pingupz}

def def_loader(file_path):
    import pandas as pd

    df = pd.HDFStore(file_path, 'r')
    return df

def load_obj(tag, file_path):

    if tag in loaders:
        raise NotImplementedError('Damn... the object_loaders is actually in use.')

        return loaders[tag](file_path)
    else:
        return def_loader(file_path)
        raise NotImplementedError('No loader for: {0}'.format(tag))
