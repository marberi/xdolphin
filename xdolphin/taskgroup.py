#!/usr/bin/env python

from . import obj_hash

class Taskgroup:
    """Create a group of tasks."""

    def _create_setattr(self, jobs_out): #, jobs_in={}):
        def _make_attr(X):
            print(self, X)
            for link_name, job in iter(X.items()):
                setattr(self, '_{0}'.format(link_name), job)

        if isinstance(jobs_out, dict):
            _make_attr(jobs_out)
        else:
            self.name = jobs_out.name
            self.config = jobs_out.config

            _make_attr(jobs_out.depend)

    def taskid(self):
        """Taskid for the taskgroup. This is determined by what
           the group contains.
        """

        X = self._jobs_out
        if hasattr(X, 'taskid'):
            return X.taskid()
        elif isinstance(X, dict):
            D = {}
            for key,job in iter(X.items()):
                D[key] = job.taskid()

            return obj_hash.hash_tree(D)
        else:
            raise ValueError('Internal error...')


    def __init__(self, jobs_out):
        self._create_setattr(jobs_out)

        self._jobs_out = jobs_out
