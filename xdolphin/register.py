#!/usr/bin/env python
# encoding: UTF8

# Interface which is used to register which results are 
# actually requested.

import pandas as pd

class DataFrame:
    def __getitem__(self, key):
        print('getitem', key)

        return self

    def join(self, X, **Y):
        return self

    def fake_repr(self):
        return 'fake_repr'

    def __getattr__(self, key):
        # First tests.

        print('getattr', key)
        if key == '__repr__':
            return self.fake_repr

        print('At the end')
        return self


    def __call__(self, *X):
        pass

class Store(object):
    def __init__(self):
        self.accessed = []

    def __getitem__(self, key):
        self.accessed.append(key)

        return DataFrame()

class Result(object):
    def __init__(self):
        self.tree = {}

    def __getitem__(self, key):
        store = Store()
        self.tree[key] = store

        return store

class Pipeline(object):
    """Fake pipeline registering all requests for
       results.
    """

    # root - Reference to the pipeline root
    # name - Where the object is in the hirarchy.
    def __init__(self, root=None, name=()):
        self.root = root if root else self
        self.name = name

        self.results = {}
 
    def getattr(self, x):
        """Normal attribute access."""
        return object.__getattribute__(self, x)

    def __getattr__(self, key):
        name = self.getattr('name')
        root = self.getattr('root')

        if key == 'result':
            result = Result()
            root.results[name] = result

            return result
        else:
            return Pipeline(root, name+(key,))


    def requested(self):
        """Return a list of results requested."""

        reg = self.getattr('results')

        return map('.'.join, reg.keys())

