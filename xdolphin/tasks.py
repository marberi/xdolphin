#!/usr/bin/env python
# encoding: UTF8

import imp
import os

# This interface is focused on loading all tasks at once. In
# reality, it would be better only loading when needing the
# tasks.

class Tasks:
    def __init__(self, conf):
        self.conf = conf
#        self.tasks = self._load_tasks()
        self._task_paths = self._find_task_paths()
        self.tasks = {}

    _internal_tasks = ['filetask', 'empty']

    def sub_conf(self):
        for proj_name, proj_conf in iter(self.conf['projects'].items()):
            if 'plots' in proj_name:
                p = proj_conf['path']
            else:
                p = os.path.join(proj_conf['path'], 'tasks')

            yield {'path': p, 'tasks': proj_conf['tasks']}

        d = os.path.dirname(__file__)
        path = os.path.join(d, 'deftasks')

        yield {'path': path, 'tasks': self._internal_tasks}

    
    def _find_task_paths(self):
        # Mapping between tasks and their paths. Should maybe
        # not be needed.
        task_paths = {}
        for proj_conf in self.sub_conf():
            proj_path = os.path.expanduser(proj_conf['path'])

            print(proj_conf['tasks'])

            for task_name in proj_conf['tasks']:
                file_name = '{0}.py'.format(task_name)
                path = os.path.join(proj_path, file_name)

                task_paths[task_name] = path

        return task_paths

    def load_all(self):
        task_path = self._find_task_paths()

        data = {}
        for task_name, path in iter(task_path.items()):
            part = {'name': task_name}

            print(path)
            mod = imp.load_source(task_name, path)
            task = getattr(mod, task_name)

            # The description.
            if hasattr(mod, 'descr'):
                descr = mod.descr
            elif hasattr(task, 'descr'):
                descr = task.descr

            part['descr'] = descr

            assert hasattr(task, 'config')
            part['config'] = task.config

            # Earlier version was not required. This needs to
            # be changed.
            version = task.version if hasattr(task, 'version') \
                      else 1.0
            part['version'] = version

            data[task_name] = part

        return data

    def test_paths(self):
        task_path = self._find_task_paths()
        for task_name, path in iter(task_path.items()):
            if not os.path.exists(path):
                print(path)

    def _cache_task_data(self):
        ipdb.set_trace()

    def __getitem__(self, task_name):
        if not task_name in self.tasks:
            if not task_name in self._task_paths:
                raise ValueError('No such task: {0}'.format(task_name))

            path = self._task_paths[task_name]
            mod = imp.load_source(task_name, path)
            self.tasks[task_name] = getattr(mod, task_name)

        return self.tasks[task_name]

    def __contains__(self, key):
        return key in self.tasks

    def keys(self):
        return self.tasks.keys()
