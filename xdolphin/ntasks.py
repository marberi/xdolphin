#!/usr/bin/env python

import imp
import os
import yaml

from .gentaskinfo import GenTaskInfo

class Tasks:
    """Lazy interface for loading the tasks."""

    def __init__(self, config):
        self.config = config
        self._task_info = self._get_task_list()

        self._tasks = {}

    def _get_task_list(self):
        """Load or generate the task info."""

        geninfo = GenTaskInfo(self.config)
        task_info = geninfo.get()

        return task_info

    def __getitem__(self, task_name):
        """Lazy loading of tasks."""

        if task_name in self._tasks:
            task = self._tasks[task_name]
        else:
            msg = 'No such task: {0}'.format(task_name)
            assert task_name in self._task_info, msg

            info = self._task_info[task_name]
            mod = imp.load_source(info['class'], info['path'])
            task = getattr(mod, info['class'])
            task.name = task_name

            self._tasks[task_name] = task

        return task
