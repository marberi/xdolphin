#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import json

from . import mainconfig
from . import runners
from . import topnode
from . import xdolphin
from .  import archive 

def create_session():
    config = mainconfig.MainConfig()
    runner = runners.Runner()
    xarchive = archive.localfiles.Archive(config)

    return Session(config, runner, xarchive)

class Session:
    def __init__(self, config, runner, archive):
        self.config = config
        self.runner = runner
        self.archive = archive

    def Job(self, name):
        return xdolphin.Job(name, self)

    def load_pipel(self, path):
        """Loads a pipeline from a file."""

        graph = json.load(open(path))

        jobsD = {}
        for jobid, data in graph.items():
            job = self.job(data['name'])
            job.config.update(data['config'])

            jobsD[int(jobid)] = job

        # Second pass to create the dependencies
        has_parent = []
        for jobid, data in graph.items():
            job = jobsD[int(jobid)]

            for dep_name, dep_jobid in data['depend'].items():
                has_parent.append(dep_jobid)
                job.depend[dep_name] = jobsD[dep_jobid]

        # These files should have been produced from a method
        # on a single job.
        ontop = set(jobsD.keys()) - set(has_parent)
        assert len(ontop) == 1
        ontop = jobsD[ontop.pop()]

        return ontop
