def obj_loader():
    if tag == 'r2':
        return np.loadtxt(file_path).T
    if tag == 'xvals':
        return np.loadtxt(file_path)

class xvals:
    """Example 1."""

    name = 'xvals'

    config = {'N': 20}

    def run(self):
        print('Calculating xvals')

        x = np.arange(self.config['N'])
        file_path = self.job.empty_file('xvals')

        np.savetxt(file_path, x)

class fisk:
    """Example 2."""

    name = 'fisk'
    config = {'a': 1, 'b': 2}

#    kids = {}

    def run(self):
        print('Executed fisk')
        file_path = self.job.empty_file('r2')

        x = self.job.kids['xvals']['xvals']
        y = self.config['a'] + self.config['b'] * x**2

        print('storing', file_path)
        np.savetxt(file_path, np.vstack([x,y]).T)

        time.sleep(1)

def example_run():
    A = Job('fisk')
    A.depend('xvals', Job('xvals'))

    for i in [1,2,3,4]:
        A.task.config['b'] = i
        A.task.config['a'] = 10+i

        print(i)
        x,y = A['r2']
