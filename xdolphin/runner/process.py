#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger

# Some of these methods should not be here, but to be part of the
# archive code...

class ProcessRunner:
    def only_run(self, job, archive):

        # This should later not be set in the first place....
        if hasattr(job.task, 'job') and not job.task.name == 'bcnz_comb_ext':
            delattr(job.task, 'job')

        # Here the running of the job depends indirectly on the archive
        # being accesible from the job because of having beeing loaded
        # 
        input_wrapper = archive.input_wrapper(job)
        output_wrapper = archive.output_wrapper(job)

        # These are closer to the new interface...
        job.task.input = input_wrapper
        job.task.output = output_wrapper

        if hasattr(job.task, 'check_config'):
            job.task.check_config()

        job.task.run()

        return output_wrapper

    def dep_order(self, job):
        """Get the order to run tasks when using a depth first order."""

        L = [job]
        for key, dep in job.depend.items():
            L.extend(self.dep_order(dep))

        return L

    def run_dep_first(self, job, archive):
        """Run in a depth first order."""

        # This will include duplicate entries, but we will have
        # to check when running anyway..
        order = self.dep_order(job)
        order.reverse()

        for i,job in enumerate(order):
            taskid = job.taskid()
            if taskid in archive.finished:
                continue

            print(len(order), 'i', i, job.name)

            output = self.only_run(job, archive)
            archive.commit(output, taskid)

    def __call__(self, job, archive, use_cache=True):
        # Passing the finished array here is not very good since
        # it would make it more difficult to run jobs in
        # parallel.

        taskid = job.taskid()
        is_cached = taskid in archive.finished
        if is_cached and use_cache:
            return

        # So far this is the only implemented version, but it
        # opens for actually running jobs in parallel.
        self.run_dep_first(job, archive)
