#!/usr/bin/env python
# encoding: UTF8

import time
from dask.distributed import LocalCluster

from .baserunner import BaseRunner

class DaskRunner(BaseRunner):
    def connect(self):
        # Since sessions are created at different points.
        if not hasattr(self, 'cluster'):
            t1 = time.time()
            self.cluser = LocalCluster()
            print('time cluser', time.time() - t1)

    def run_dep_first(self, job, archive):
        """Run in a depth first order."""


        # This is a bad hack, but can work in some cases....
        L = []
        for key, dep in job.depend.items():
            L.append(dask.delayer(dep.run)())

        dask.compute(L)
        print('In the depth first run..')
