#!/usr/bin/env python
# encoding: UTF8

def add_task(obj):
    """Add task to the task list."""

    import xdolphin

    # This could probably be done in a better way...
    assert hasattr(obj, 'name'), 'Missing attribute: name'

    xdolphin.tasks[obj.name] = obj

def add_loader(tag, loader):
    """Add object loader."""

    import xdolphin
    xdolphin.obj_loaders.loaders[tag] = loader

def config():
    import config

    return config.config()

def plot_path(name):
    ipdb.set_trace()

class Common:
    """Placeholder for jobs."""

    def __init__(self, name):
        self.name = name


import imp

from . import archive
from .xdolphin import Job, Config
from .topnode import Topnode

from .taskgroup import Taskgroup
from .layers import Layers

from . import store
store_fig = store.store_fig

from .load_pipel import load_pipel, mk_pipel
from .scratch import cache_call

from .fakejob import as_job

from .plot import plot

from .session import create_session as session

def asnb(name):
    # Still early and experimental. The idea is to
    # create an interface which simpler can be loaded
    # in the notebooks.
    import os
    import pdb

    
    mod = imp.load_source(name, name+'.py')
    inst = mod.ForNB()

    return inst
