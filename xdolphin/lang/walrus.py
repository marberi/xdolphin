#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger as ipdb
import os

def parse_cmds(fpath):
    cmds = []
    for line in open(fpath):
        # Both commented out and empty lines are
        # ignored.
        if line.startswith('#'):
            continue

        spl = line.strip().split()
        if not len(spl):
            continue

        # Ignore multiple spaces
        spl = list(filter(lambda x: len(x), spl))

        cmds.append(spl)

    return cmds

class MakeLinks:
    """Create links between the run and links directory. Used for
       not editing articles when updating the figures.
    """

    def __init__(self, run_dir, links_dir):
        self.links_dir = links_dir
        self.run_dir = run_dir

    def _mklink(self, src_name, dst_name):
        """Actually do the linking."""

        src = os.path.join(self.run_dir, src_name)
        dst = os.path.join(self.links_dir, dst_name)

        assert os.path.exists(src), 'File not existing: {0}'.format(src)
        if os.path.exists(dst):
            assert os.path.islink(dst), 'What is this: {0}'.format(dst)

            if os.path.realpath(src) == os.path.realpath(dst):
                return 'exists'
            else:
                os.remove(dst)

        os.symlink(src,dst)
        return 'made link'


    def _cmd_sep(self, X, suf='pdf'):
        """The most common format."""

        src_name = '{0}.{1}'.format('_'.join(X), suf)
        dst_name = 'prod_{0}.{1}'.format('_'.join(X[1:]), suf)

        return src_name, dst_name


    def _cmd_tmphack(self, X):
        """Direct link."""

        name = X[0]

        return name, name

    def _cmd_table(self, X):
        return self._cmd_sep(X, 'csv')


    def _cmd_txt(self, X):
        return self._cmd_sep(X, 'txt')

    def __call__(self, cmds):

        f_cmd = {'newfig': self._cmd_sep,
                 'newbias': self._cmd_sep,
                 'newtable': self._cmd_table,
                 'newtxt': self._cmd_txt,
                 'tmphack': self._cmd_tmphack}

        res = []
        for cmd in cmds:
            src_name, dst_name = f_cmd[cmd[0]](cmd[1:])
            res.append(self._mklink(src_name, dst_name))

        return res
