#!/usr/bin/env python
# encoding: UTF8

import pandas as pd

def _split_cols_part(sub, pre):
    """How to rename the columns."""
    renameD = {}

    tpre = pre+'_'
    for col in sub.columns:
        assert not isinstance(col, tuple), 'Passed catalog has hirarchical columns'

        if not col.startswith(tpre):
            continue

        new_col = col.replace(tpre,'')
        if '_' in new_col:
            continue

        renameD[col] = (pre, new_col)

    return renameD

def split_cols(cat, prefs, part_name=None):
    """Move some columns into a hirarchical set."""

    # prefs - Prefixes to move.
    # part_name - Name of subset with problem

    # How to rename and the renaming is splitted in two since
    # one later might want to create and interface to handle
    # this more generally.
    sub = cat[part_name] if part_name else cat
    for pre in prefs:
        partD = _split_cols_part(sub, pre)

        keys, vals = zip(*iter(partD.items()))
        keys = list(keys)
        inds = pd.MultiIndex.from_tuples(vals)

        # Index hell.
        cat[inds] = sub[keys]
        oinds = pd.MultiIndex.from_product(['galcat',keys])
        cat.drop(oinds, axis=1, inplace=True)

def fixcols(df, names):
    """Fix selected columns which should be multiindex."""

    # The dataframe is constructed by appending series. In these
    # the entries in names should be multi-index. Unfortunately
    # this was difficult with the Series. This postprocessing
    # step fixes the dataframe.

    def subset(name):
        F = pd.concat(df[name].tolist(),axis=1).T
        F.columns = pd.MultiIndex.from_product([name, F.columns])

        return F

    # Combining a normal indexed and multi-index dataframe was
    # also difficult. Creating a new multi-index dataframe seems
    # to be the best.
    H = pd.concat(map(subset, names), axis= 1)    
    tocopy = list(set(df.columns) - set(names))
    H[tocopy] = df[tocopy]

    return H
