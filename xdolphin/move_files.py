
# Example code for taking backup of the important files!

def move_files():
    nodes, dep = pipel.data()
    nodes = nodes[nodes.name != 'empty']

    from_d = '/home/eriksen/.xdolphin/archive/default'
    to_d = '/home/eriksen/.xdolphin/new_files'
    for taskid in nodes.taskid.unique():
        from_path = os.path.join(from_d, taskid)

        assert os.path.exists(from_path)

        to_path = os.path.join(to_d, taskid)
        os.link(from_path, to_path)


