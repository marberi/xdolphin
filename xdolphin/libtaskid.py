#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import copy
from . import obj_hash

def find_dep(job):
    """Recursively build up a tree of the jobs."""

    # Since estimating the taskid became extremely expensive when
    # travelsing a large tree.
    def _core(job, dep, has_seen):
        if job.jobid in has_seen:
            return

        dep[job.jobid] = {(key,job.jobid) for (key, job) in job.depend.items()}
        has_seen[job.jobid] = job

        for key, job in job.depend.items():
            _core(job, dep, has_seen)

    dep = {}
    has_seen = {}
    _core(job, dep, has_seen)

    return dep, has_seen

def jobs_graph(job):
    """Flat version of the pipeline."""

    def _core(job, jobsD):
        if job.jobid in jobsD:
            return

        data = {}
        data['name'] = job.name
        data['config'] = job.config.task_config
        data['depend'] = {k:v.jobid for k,v in job.depend.items()}
        jobsD[job.jobid] = data

        for dep in job.depend.values():
            _core(dep, jobsD)

    jobsD = {}
    _core(job, jobsD)

    return jobsD

def determine_order(dep):
    """Determines the order to construct the hashes."""

    order = []

    # This method relies on modifying dep.
    dep = copy.deepcopy(dep)

    # Remove the dependency name (x[0])
    rm_depname = lambda dep_val: set(x[1] for x in dep_val)
    dep = {k: rm_depname(v) for (k,v) in dep.items()}

    while True:
        nr = {k:len(v) for k,v in dep.items()}
        nr_sum = sum(nr.values())
        if nr_sum == 0:
            order.append(set(nr.keys()))

            return order

        torem = {k for (k,v) in nr.items() if v == 0}
        order.append(torem)
        for k in torem:
            del dep[k]

        dep = {k: v-torem for k,v in dep.items()}

def estim_taskid(order, jobsD):
    """Estimte the taskid."""

    order = sum(map(list, order), [])

    taskidD = {}
    for jobid in order:
        job = jobsD[jobid]

        # And then estimating the taskid for this job.
        config_hash =  obj_hash.hash_tree(job.task.config)

        dep_hashL = []
        for name,k in iter(job.depend.items()):
            dep_hashL.append((name, taskidD[k.jobid]))

        dep_hashL.sort()
        to_hash = {'name': job.task.name,
                   'version': job.task.version,
                   'config': config_hash,
                   'dep': dep_hashL}

        taskidD[jobid] = obj_hash.hash_tree(to_hash)

    return taskidD
