#!/usr/bin/env python

import copy

class PipelineConf:
    """Configuration set for an entire pipeline."""

    def __init__(self, config={}, order=[], labels={}):
        self._config = config
        self._order = order
        self._labels = labels

        # Note: Order is kept separate from _config so
        # one later can permute the order.

    def _keep_order(self, name):
        """Keep track of when the different names are
           added.
        """

        if not name in self._config:
            self._config[name] = {}
            self._labels[name] = {}

        if not name in self._order:
            self._order.append(name)


    def set_val(self, name, key, val, label=None):
        """Set a single value."""

        self._keep_order(name)
        self._config[name][key] = val

        if isinstance(label, type(None)):
            label = val

        self._labels[name] = (name, label)

    def update(self, name, D, label):
        """Update given a dictionary."""

        self._keep_order(name)
        self._config[name].update(D)

        self._labels[name] = (name, label)

    def clone(self):
        """Clone with deep copy of internal data."""

        _config = copy.deepcopy(self._config)
        _order = copy.deepcopy(self._order)
        _labels = copy.deepcopy(self._labels)

        return PipelineConf(_config, _order, _labels)

    def flat_conf(self):
        """Flat version of the configuration structure."""

        conf = {}
        for name in self._order:
            conf.update(self._config[name])

        return conf

    def flat_labels(self):
        """Flat version of the label structure."""

        labels = {}
        for name in self._order:
            lbl_key, lbl_val = self._labels[name]
            labels[lbl_key] = lbl_val

        return labels
