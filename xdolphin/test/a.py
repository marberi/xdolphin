#!/usr/bin/env python
# encoding: UTF8

from IPython.core import debugger
import imp
import os
import time
import tempfile

# Not sure where this should be located...
def setup(config):
    for d in config['dirs']:
        if not os.path.exists(d):
            os.makedirs(d)


def extact_task_info(path, task_name):
    """Extract the task information from a task class."""

    mod = imp.load_source(task_name, path)
    cls = getattr(mod, task_name)

    info = {'name': task_name}

    for field in ['version', 'config']:
        msg = '{0} task has no {1} attribute'
        assert hasattr(cls, field), msg.format(task_name, filed)

        info[field] = getattr(cls, field)

    assert hasattr(cls, '__doc__'), \
        '{0} has no docstring'.format(task_name)

    info['doc'] = cls.__doc__
    info['path'] = path
    info['class'] = name
 
    return info 

#setup(config)
t1 = time.time()
import mainconfig
import gentaskinfo
t2 = time.time()
print('time importing', t2-t1)

t1 = time.time()
config = mainconfig.MainConfig()
t2 = time.time()

print('time conf', t2-t1)
#info = gentaskinfo.GenTaskInfo(config)
#info.write()

import taskinfo
info = taskinfo.TaskInfo(config)
info.load()

ipdb.set_trace()
