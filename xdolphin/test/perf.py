#!/usr/bin/env python
# Some simple performance tests.

import timeit

# For loading the full configuration.
A = timeit.timeit('mainconfig.MainConfig()', 'import mainconfig', number=11)

print(A)
