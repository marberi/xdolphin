#!/usr/bin/env python

import collections
import copy
import itertools as it

from .xdolphin import Job
from .topnode import Topnode

from .pipelineconf import PipelineConf

class BasicLayers:
    """The basic methods for expanding layers, but without
       any conventions.
    """

    # Subclass this to add your own layers.

    def _layer_order(self, k1, k2):
        """Order the layers by using sort."""
        
        # Sorting the layers to follow the order given in
        # self.order. This was simpler than an if-else
        # construct and is more general.

        def getnr(x):
            if x in self.order:
                return self.order.index(x)
            else:
                return 100

        v1 = getnr(k1)
        v2 = getnr(k2)

        return v1-v2

    def to_conf(self):
        """Expand the pipelines to their configuration."""

        current = [PipelineConf()]
        
        layer_keys = self._data.keys()
        layer_keys.sort(cmp=self._layer_order)

        for lkey in layer_keys:
            D = self._data[lkey]
            op = D['op']

            # This is not correct, but I am not ready to add this
            # here yet.
            if 'pipel' in op:
                continue

            # Here we use a convetion instead of registering
            # each function.
            op_key = '_expand_{0}'.format(op)
            f_op = getattr(self, op_key)

            print('op', op, 'D', D)
            current = f_op(current, D)

        confs = dict(enumerate(current))

        return confs 

    def to_pipelines(self):
        """Expand the pipelines."""

        # Later this should hopefully not be needed

        assert not (('pipel' in self._data) and ('pipel_layer' in self._data)), \
               'Error: both pipel and pipel_layers are set'

        if 'pipel' in self._data:
            pipelD = {0: self._data['pipel']['value']}
        elif 'pipel_layer' in self._data:
            pipelD = self._data['pipel_layer']['value']
        else:
            raise ValueError('No pipelines are defined')

        return pipelD

    def to_topnode(self):
        """Create a topnode based on this layer."""

        confs = self.to_conf()
        pipelD = self.to_pipelines()
        top = Topnode(pipelD, confs)

        return top


class Layers(BasicLayers):
    """Cartesian product of configuration options."""

    # This code store the transformations and first later
    # expand them.

    # New version of the layers code which stores the full information
    # about what is added and then expands later. 
    _data = collections.OrderedDict()

    order = ['config', 'pipel', 'pipel_layer']

    # The ordering of the following code is:
    # - Method for registering operation
    # - Method for expanding operation
    #
    # for each of the different operations.

    # Configuration
    def add_config(self, config):
        """Add the configuration."""

        D = {'op': 'add_config', 'value': config}
        assert isinstance(config, dict), 'The configuration should be a dictionary'

        self._data['config'] = D

    def _expand_add_config(self, current, D):
        """Expand the configuration."""

        assert len(current) == 1, 'One only expect one configuration here'

        pipel = current[0]
        pipel.update('conf', D['value'], 'conf')

        return [pipel]
    
    # Single pipeline
    def add_pipeline(self, pipel):
        """Add a single pipeline."""

        D = {'op': 'add_pipel', 'value': pipel}
        self._data['pipel'] = D

    def _expand_add_pipel(self, current, D):
        """Expand the single pipeline operation."""

        raise NotImplementedError('not used yet')

    # Layer of pipelines.
    def add_pipeline_layer(self, pipel_in, labels=None):
        """Add a layer of different pipelines."""

        assert not labels, 'Implement this..'

        if isinstance(pipel_in, dict):
            D = {'op': 'add_pipel_layer', 'value': pipel_in.copy()}
            self._data['pipel_layer'] = D
        else:
            raise NotImplementedError('should this be implemented?')

    def _expand_pipel_layer(self, currend, D):
        raise NotImplementedError('not used yet')

    # Add layer defined by key
    def add_layer_key(self, name, key, values, labels=None):
        # Note: This should use the add_layer method??
        D = {'op': 'add_layer_key', 'name': name, 'key': key, 'values': values,
             'labels': labels}

        self._data[name] = D

    def _expand_add_layer_key(self, current, D):
        """Expand the configuration when a layer is specified by a
           key.
        """

        nstate = []
        for val in D['values']:
            for pipelconf in current:
                npipel = pipelconf.clone()
                npipel.set_val(D['name'], D['key'], val)
                nstate.append(npipel)

        return nstate

    def add_layer(self, name, layer, labels=None): 
        """Add general layer."""

        assert isinstance(layer, list) or isinstance(layer, tuple), \
               'The layer should either be a list or a tuple'

        D = {'op': 'add_layer', 'name': name, 'layer': layer,
             'labels': labels}

        self._data[name] = D

    def _expand_add_layer(self, current, D):
        """Expand the configuration for a layer."""

        nstate = []

        labels = D['labels']
        if isinstance(labels, type(None)):
            labels = range(len(D['layer']))

        # This should be tested earlier?
        msg_lbl = 'Different number of labels and entries in the layer'
        assert len(labels) == len(D['layer']), msg_lbl

        for val,lbl in zip(D['layer'], labels):
            for pipelconf in current:
                npipel = pipelconf.clone()
                npipel.update(D['name'], val, lbl)
                nstate.append(npipel)

        return nstate

