#!/usr/bin/env python

import imp
import os
import shutil
import tempfile
import yaml

class GenTaskInfo:
    """Compiles together and cache task info."""

    def __init__(self, config):
        self.config = config

        cache_dir = self.config['dirs']['interf']
        self.combined_path = os.path.join(cache_dir, 'combined.yaml')

    def _extract_task_info(self, path, task_name):
        """Extract the task information from a task class."""


        mod = imp.load_source(task_name, path)
        cls = getattr(mod, task_name)

        info = {'name': task_name}

        for field in ['version', 'config']:
            msg = '{0} task has no {1} attribute'
            assert hasattr(cls, field), msg.format(task_name, field)

            info[field] = getattr(cls, field)

        assert hasattr(cls, '__doc__'), \
            '{0} has no docstring'.format(task_name)

        info['doc'] = cls.__doc__
        info['path'] = path
        info['class'] = task_name

        info['descr'] = mod.descr if hasattr(mod, 'descr') else {}

        return info


    def all_info(self):
        """Read in all the task information."""

        infoD = {}
        for proj, proj_info in iter(self.config['projects'].items()):
            task_dir = proj_info['task_dir']

            for f in os.listdir(task_dir):
                if not f.endswith('.py'):
                    continue
                if f.startswith('test_'): 
                    continue

                path = os.path.join(task_dir, f)
                task_name = f.replace('.py', '')

                # 
                if task_name in ['libpzqual', 'libpzcore']:
                    continue

                infoD[task_name] = self._extract_task_info(path, task_name)

        return infoD

    def _write_info_task(self, cache_task, info):
        """Store all the cache information to files."""

        rm_fields = []
        os.makedirs(cache_task)

        # Individual files..
        # Note that explicit closing is needed for PyPy
        for field in ['config', 'descr']:
            fb = open(os.path.join(cache_task, field), 'w')
            yaml.dump(info[field], fb)
            fb.close()

            rm_fields.append(field)

        # How to execute the code (path, etc)
        execD = {}
        for field in ['path', 'class']:
            execD[field] = info[field]
            rm_fields.append(field)

        fb = open(os.path.join(cache_task, 'exec'), 'w')
        yaml.dump(execD, fb)
        fb.close()

        otherD = {}
        for field in set(info.keys()) - set(rm_fields):
            otherD[field] = info[field]

        # Rest of the fields.
        fb = open(os.path.join(cache_task, 'other'), 'w')
        yaml.dump(otherD, fb)
        fb.close()

    def _write_info(self, infoD):
        """Store all the cache information to files."""

        tmp_dir = tempfile.mkdtemp()
        for task_name, info in iter(infoD.items()):
            dir_task = os.path.join(tmp_dir, task_name)
            self._write_info_task(dir_task, info)

        interf_dir = self.config['dirs']['interf']

        # This is not exactly handling being interrupted.
        if os.path.exists(interf_dir):
            tmp_dir2 = tempfile.mkdtemp()
            shutil.move(interf_dir, tmp_dir2)

        shutil.move(tmp_dir, interf_dir)

    def _write_combined(self, infoD):
        """Stores all the node information together in one
           file.
        """

        fb = open(self.combined_path, 'w')
        yaml.dump(infoD, fb)
        fb.close()

    def _write(self, infoD):
        """Write the information to temporary files."""

        self._write_info(infoD)
        self._write_combined(infoD)

    def gen_cache(self):
        """Generate the task cache."""

        print('Generating task information')
        infoD = self.all_info()
        self._write(infoD)

        return infoD

    def get(self):
        """Get the information and and cache if not existing."""

        if os.path.exists(self.combined_path):
            fb = open(self.combined_path)
            infoD = yaml.load(fb)
            fb.close()
        else:
            infoD = self.gen_cache()

        return infoD
