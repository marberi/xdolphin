import os
import yaml

# Default values of the directories.
from . import def_dirs

class MainConfig(dict):
    """Handle the main configuration."""

    def __init__(self, config_file=False):
        self.update(self._load_config(config_file))
        self._expand_dirs()
        self._add_dirs()
        self._check_projects()
        self._expand_home()

    def _load_config(self, path):
        """Loads the main configuration file."""

        if not path:
            path = os.path.expanduser('~/.config/xdolphin/xdolphin.conf')

        assert os.path.exists(path), \
               RuntimeError('No such configuration: {0}'.format(path))

        config = yaml.load(open(path).read())

        return config

    def _expand_dirs(self):
        """Move the directory variables into a separate
           dictionary.
        """

        dirs = {}

        # Two step since Python dislike deleting keys in
        # dictionaries it iterates over.
        tomove = []
        for key, val in iter(self.items()):
            if key.endswith('_dir'):
                tomove.append(key)

        for key in tomove:
            nkey = key.split('_')[0]
            dirs[nkey] = self[key]
            del self[key]

        assert not 'dirs' in self
        self['dirs'] = dirs

    def _add_dirs(self):
        """Add defaults if not being set."""

        dirs = self['dirs']
        for key, def_val in def_dirs.default_dirs:
            # Allows useds to set directories relative to other
            # paths.
            if (key in dirs) and not isinstance(dirs[key], tuple):
                continue

            X = dirs[key] if key in dirs else def_val
            if isinstance(X, tuple):
                path = os.path.join(dirs[X[0]], X[1])
            else:
                path = X

            dirs[key] = path


    def _check_projects(self):
        """Checks the projects are correctly configured and 
           expands some defaults.
        """

        if not 'projects' in self:
            self['projects'] = {}
            return

        for proj,data in iter(self['projects'].items()):
            msg = 'Missing path for project: {0}'.format(proj)
            assert 'path' in data, msg

            path = os.path.expanduser(data['path'])
            data['path'] = path
            assert os.path.exists(path), path

            # Not all projects have tasks, nor figures.
            if not 'task_dir' in data:
                data['task_dir'] = os.path.join(path, 'tasks')

    def _expand_home(self):
        """Expand the home directory."""

        for key,val in iter(self['dirs'].items()):
            self['dirs'][key] = os.path.expanduser(val)

