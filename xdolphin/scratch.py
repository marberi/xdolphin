#!/usr/bin/env python

import os
import inspect
import hashlib

import pandas as pd

def cache_call(f):
    """Cache a call to a plotting method."""

    # This function caches the result from a call
    # to a method on a plotting class. It assumes
    # that topnodes are always used and the only
    # part except the method source code which
    # needs versioning.

    # Something more sophisticated can be added
    # if I continue using this functionality.

    def wrapper(self, *args):
        print(self, args)

        top = self.topnode()
        topid = top.topid()

        source = inspect.getsource(f)
        version = hashlib.sha1(source).hexdigest()

        print('topid', topid)
        print('version', version)

        name = '{0}_{1}'.format(version, topid)
        d = '/data2/marberi/results/scratch'

        path = os.path.join(d, name)
        print(path)
        if os.path.exists(path):
            with pd.HDFStore(path) as store:
                res = store['scratch']
        else:
            res = f(self, *args)

            try:
                store = pd.HDFStore(path, 'w')
                store['scratch'] = res
                store.close()
            except TypeError as e:
                store.close()
                os.remove(path)

                raise TypeError('Return an object Pandas can store')

        return res

    return wrapper
