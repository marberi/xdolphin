#!/usr/bin/env python

class Fakejob(object):
    """Interface for testing code before separating it into a 
       different class.
    """

    def __init__(self, old_job):
        #self.depend = depend

        assert hasattr(old_job, 'depend'), 'Job without dependencies..'

        for key,job in iter(old_job.depend.items()):
            setattr(self, key, job)


def as_job(cls, old_job):
    """Wrap task cls as a job and add dependencies from old job."""

    inst = cls()
    inst.job = Fakejob(old_job)

    for attr in ['config', 'version']:
        msg = '{} is a required task attribute'.format(attr)
        assert hasattr(inst, attr), msg

    return inst
