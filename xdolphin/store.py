#!/usr/bin/env python
# encoding: UTF8

def add_metadata(in_path, out_path, meta):
    """Add xdolphin meta data to the pdf file."""

    import pdfrw
    fin = pdfrw.PdfReader(in_path)
    fin.Info.xdolphin = meta

    fout = pdfrw.PdfWriter(out_path)
    fout.addpages(fin.pages)
    fout.trailer.Info = fin.Info
    fout.write(out_path)

def store_fig(plt, path, topnode):
    """Save the figure together with the job information."""

    import tempfile

    meta = topnode.tojson()
    tmp_path = '{0}.pdf'.format(tempfile.mktemp())
    plt.savefig(tmp_path)
    add_metadata(tmp_path, path, meta)
