#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import pdb
import hashlib
import os
import time
import pandas as pd
import numpy as np

class FrozenDict(dict):
    """Frozen dictionary."""

    # Implement this here since its only available on newer Python
    # versions.
    def __warning__(self, *args, **kwargs):
        raise RuntimeError('Dictionary modifications are not allowed')

    __setitem__ = __warning__
    __delitem__ = __warning__
    setdefault = __warning__
    pop = __warning__
    popitem = __warning__
    clear = __warning__
    update = __warning__


class FrozenList(list):
    """Frozen list."""

    # Implement this here since its only available on newer Python
    # versions.
    def __warning__(self, *args, **kwargs):
        raise RuntimeError('Dictionary modifications are not allowed')

    __setitem__ = __warning__
    __delitem__ = __warning__
    setdefault = __warning__
    pop = __warning__
    popitem = __warning__
    clear = __warning__
    update = __warning__
    

class Topnode:
    """Interacting with a set of pipelines."""

    def _fix_format(self, pipelD, confD):

        # Another compatible layer from updating the format
        nodes = {}
        labels = pd.DataFrame()

        i = 0
        for key_pipel, pipel in iter(pipelD.items()):
            for key, conf in iter(confD.items()):
                npipel = pipel.clone()
                npipel.config.update(conf.flat_conf())
                nodes[i] = npipel
                print('i', i)

                lbl_part = pd.Series(conf.flat_labels(), name=i) #key)
                lbl_part['base_pipel'] = key_pipel

                labels = labels.append(lbl_part)
                i += 1
        
        labels.index = labels.index.set_names('pipel')
        labels['base_pipel'] = labels.base_pipel.astype(np.int)

        return nodes, labels

    def __init__(self, pipel, confs):
        nodes, labels = self._fix_format(pipel, confs)

        self.nodes = FrozenDict(nodes)
        self.labels = labels

    def topid(self):
        """ID for the full topnode."""

        # Ordering the keys is important 
        tohash = []
        for key in sorted(self.nodes.keys()):
            pipel = self.nodes[key]
            tohash.append((key, pipel.taskid()))

        topid = hashlib.md5(str(tohash)).hexdigest()

        return topid

    def clone(self):
        """Clone the topnode."""

        raise NotImplementedError('Ok, it is.. but should be deprecated..')

        new_nodes = {}
        for key, val in iter(self._nodes.items()):
            new_nodes[key] = val.clone()

        # Total abuse of the API..
        from xdolphin import Job
        xtop = self.__class__(Job('empty'))
        xtop._nodes = new_nodes

        return xtop


    def all_results(self, version):
        """All results needed for the pipeline."""

        allresD = dict((key,x.taskid()) for (key, x) in iter(self.nodes.items()))
        allresT = [(key,x.taskid()) for (key, x) in iter(self.nodes.items())]

        allres = pd.DataFrame(allresT, columns=['pipel', 'taskid'])
        allres['version'] = version
        allres = allres.set_index('pipel')

        return allres

    def stored_results(self, all_results):
        """Which of these results are stored.""" 

        dir_prepare = self.session.config['dirs']['prepare']

        stored = pd.DataFrame(index=all_results.index)
        for key,row in all_results.iterrows():
            fname = '{0}_{1}'.format(row.version, row.taskid)
            path = os.path.join(dir_prepare, fname)

            stored.ix[key, 'path'] = path
            stored.ix[key, 'exists'] = os.path.exists(path)

        stored['exists'] = stored.exists.astype(bool)

        return stored

    def _calculate(self, f_map, to_calc, cache, stored):
        """Calculate the prepare phase for all entries."""

        allprepD = {}
        for key,row in to_calc.iterrows():
            pipel = self.nodes[key]

            res = f_map(pipel)
            if cache:
# Testing on object is not sufficient. Objects with one type and nans
# appear as the object dtype, but give problem when storing the file.
#                assert not np.dtype('O') in res.dtypes.values, \
#                   'One object has object dtype. This will give problem storing the dataframe.'

                with pd.HDFStore(stored.ix[key].path, 'w') as store:
                    store.append('prepare', res)

            allprepD[key] = res

        return allprepD

    def _fetch(self, to_fetch):
        """Fetch cached results."""

        allfetchD = {}
        for key,row in to_fetch.iterrows():
            print(row.path)
            with pd.HDFStore(row.path) as store:
                res = store['prepare']

            allfetchD[key] = res

        return allfetchD
 
    def XXX_labels(self, labels):
        """Create the labels dataframe."""

        msg = 'it is implemented, but moved away..'

        msg = 'it is implemented, but moved away..'

        raise NotImplementedError(msg)

        lbl_df = pd.DataFrame()
        for key in self.nodes.keys():
            pipel = self.nodes[key]

            if callable(labels):
                part = labels(pipel)
            elif isinstance(labels, dict):
                part = pd.Series()
                for key2, val2 in iter(labels.items()):
                    part[key2] = pipel.config[val2]

            part['key'] = key
            lbl_df = lbl_df.append(part, ignore_index=True)


        # Also keeps the old index to be able to simply change
        # index.
        lbl_df.index = pd.MultiIndex.from_tuples(lbl_df.key)

        return lbl_df

    def _version_code(self, f_map):
        """Create a version for the code by inspecting the source."""

        import inspect
        import hashlib

        source = inspect.getsource(f_map)
        version = hashlib.sha1(source).hexdigest()

        return version

    def _store_calculated(self):
        """Store information about the pipelines which
           has been calculated.
        """

        # Since I can not start doing this at once.
        if not hasattr(self, 'session'):
            return

        t1 = time.time()

        meta_dir = self.session.config['dirs']['meta']
        stored = os.listdir(meta_dir)

        for key, pipel in iter(self.nodes.items()):
            taskid = pipel.taskid()
            if taskid in stored:
                print('already stored')
                continue

            print('actually storing..')
            pipel.save(meta_dir)

        print('time store', time.time() - t1)

    def reduce(self, f_map, cache=False):
        """Map f_map onto each pipeline and combine the data."""

        version = self._version_code(f_map)
        all_results = self.all_results(version)

        self._store_calculated()


        if cache:
            stored = self.stored_results(all_results)

            to_fetch = stored[stored.exists]
            to_calc = stored[~stored.exists]
            allcalcD = self._calculate(f_map, to_calc, cache, stored)
            allfetchD = self._fetch(to_fetch)

            toconcat = allcalcD.copy()
            toconcat.update(allfetchD)
            allprep = pd.concat(toconcat, names=['pipel'])
        else:
            FF = pd.DataFrame()
            allprepD = self._calculate(f_map, all_results, cache, FF)
            allprep = pd.concat(allprepD, names=['pipel'])

        allprep = pd.DataFrame(allprep)
        allprep = allprep.join(self.labels)

        return allprep

    def run(self):
        """Enforce running of all pipelines."""

        # Experimental support for running the different results.
        # Currenly only executing the tasks in serial.

        for key, pipel in iter(self._nodes.items()):
            pipel.run()


    # It is unsure if these should be here, but rec_apply is in
    # use a few places.
    def _apply_pipel(self, root, f_app):
        """Then applyin the function recursively."""

        f_app(root)
        for key, dep_job in iter(root.depend.items()):
            self._apply_pipel(dep_job, f_app)

    def rec_apply(self, f_app):
        """Recursively apply function to all nodes."""

        for key,pipel in iter(self.nodes.items()):
            self._apply_pipel(pipel, f_app)


    def _pipel_dep(self, job, root=[], dep={}):
        #dep['.'.join(root)] = job.taskid()
        dep['.'.join(root)] = job.jobid #taskid()
        for dep_name, dep_job in iter(job.depend.items()):
            self._pipel_dep(dep_job, root+[dep_name], dep)

        return dep
